<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/category/{slug}', 'CategoryController@show');

Route::get('/product/{slug}', 'ProductController@show');

Route::get('/add-to-cart/{slug}', 'CartController@AddToCart');
Route::get('/shopping-cart', 'CartController@ShowCart');
Route::get('/delete-to-cart/{slug}', 'CartController@DeleteOneItemCart');
Route::get('/delete-all-to-cart', 'CartController@DeleteAllItemCart');

Route::get('/locale/{locale}', [
    'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@index'
]);

Route::post('/subscription', 'SubscriptionController@store');
Route::post('/comment', 'CommentController@store');
Route::post('/search', 'SearchController@store');