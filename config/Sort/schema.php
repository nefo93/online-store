<?php

return [
    'schema' => [
        'default' => [
            'sort' => \App\Services\Search\Composite\CompositeSort::class,
        ],
        'sort-products' => [
            'sort' => \App\Services\Search\Composite\CompositeSort::class,
            'items' => [
                \App\Services\Search\Sort\ByCategoryAndProductPrice::class,
            ]
        ],
    ]
];