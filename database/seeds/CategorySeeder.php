<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $n = null;
        for ($i = 1; $i <= 15; $i++) {
            if($i > 5) $n = rand(1, 5);
            $insert[] = [
                'parent_id' => $n,
                'name' => 'Category '.$n.'-'.$i,
                'slug' => 'slug-'.$i,
                'description' => 'Description'.$n.'-'.$i
            ];
        }

        DB::table('categories')->insert($insert);
    }
}
