<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    const COUNT = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'category_id' => rand(6, 15),
                'name' => 'Product-'.$i,
                'slug' => 'slug-'.$i,
                'description' => 'Description-'.$i,
                'price' => rand(0, 10000),
                'status' => rand(0, 1),
                'quantity' => rand(0, 1000)
            ];

        }

        DB::table('products')->insert($insert);
    }
}
