<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++) {
            $insert[] = [
                'imageble_id' => $i,
                'imageble_type' => 'App\Model\Product',
                'src' => 'images/products_images/product-'.$i.'.jpg',
            ];
        }

        DB::table('images')->insert($insert);
    }
}
