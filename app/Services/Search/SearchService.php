<?php

namespace App\Services\Search;


use App\Model\Product;
use App\Services\Search\Composite\Contracts\SortInterface;
use App\Services\Search\Composite\Factory as SortFactory;

class SearchService
{
    const SORT_ALIAS = 'sort-products';

    private $sortFactory;

    private $product;

    public function __construct(SortFactory $factory, Product $product)
    {
        $this->sortFactory = $factory;
        $this->product = $product;
    }

    public function collect(string $search)
    {
        $categories = $this->getProducts($search);
        try {
            $sort = $this->getSort();
            return $sort->sort($categories);
        } catch (\Exception $exception) {
            return $categories;
        }
    }

    private function getSort(): SortInterface
    {
        return $this->sortFactory->buildSort(self::SORT_ALIAS);
    }

    private function getProducts(string $search)
    {
        if($search) {
            return $this->product->where([['name', 'LIKE', '%' . $search . '%']])->with('category')->get();
        }

        return $this->product->with('category')->get();
    }

}