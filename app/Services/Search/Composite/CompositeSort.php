<?php

namespace App\Services\Search\Composite;

use App\Services\Search\Composite\Contracts\CompositeInterface;
use App\Services\Search\Composite\Contracts\SortInterface;
use Illuminate\Support\Collection;

/**
 * Class CompositeSort
 * @package App\Services\Search\Composite
 */
class CompositeSort implements CompositeInterface, SortInterface
{
    /**
     * @var array
     */
    private $sort = [];

    /**
     * @param SortInterface $sort
     * @return mixed|void
     */
    public function addSort(SortInterface $sort)
    {
        $this->sort[] = $sort;
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function sort(Collection $collection): Collection
    {
        foreach ($this->sort as $sort) {
            $collection = $sort->sort($collection);
        }

        return $collection;
    }
}