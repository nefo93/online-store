<?php

namespace App\Services\Search\Composite\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface SortInterface
 * @package App\Services\Search\Composite\Contracts
 */
interface SortInterface
{
    /**
     * @param Collection $collection
     * @return Collection
     */
    public function sort(Collection $collection);
}