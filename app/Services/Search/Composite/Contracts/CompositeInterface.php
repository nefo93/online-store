<?php

namespace App\Services\Search\Composite\Contracts;

/**
 * Interface CompositeInterface
 * @package App\Services\Search\Composite\Contracts
 */
interface CompositeInterface
{
    /**
     * @param SortInterface $sort
     * @return mixed
     */
    public function addSort(SortInterface $sort);
}