<?php

namespace App\Services\Search\Composite;


use App\Services\ConfigUploader\ConfigUploaderInterface;
use App\Services\Container\ContainerInterface;
use App\Services\Search\Composite\Contracts\CompositeInterface;
use App\Services\Search\Composite\Contracts\SortInterface;

/**
 * Class Factory
 * @package App\Services\Search\Composite
 */
final class Factory
{
    /**
     *
     */
    const CONFIG_PATH = 'Sort.schema.schema';
    /**
     *
     */
    const DEFAULT_SCENARIO = 'default';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $schema = [];

    /**
     * Factory constructor.
     * @param ContainerInterface $container
     * @param ConfigUploaderInterface $uploader
     */
    public function __construct(ContainerInterface $container, ConfigUploaderInterface $uploader)
    {
        $this->container = $container;
        $this->configure($uploader);
    }

    /**
     * @param ConfigUploaderInterface $configUploader
     */
    private function configure(ConfigUploaderInterface $configUploader)
    {
        $configs = $configUploader->getConfigs(self::CONFIG_PATH);
        $this->schema = $configs ?: [];
    }

    /**
     * @param string $alias
     * @return SortInterface
     */
    public function buildSort(string $alias): SortInterface
    {
        try {
            return $this->proceed($alias);
        } catch (\Throwable $exception) {
            dd('Error in BuildSort');
        }
    }

    /**
     * @param string $alias
     * @return SortInterface
     */
    private function proceed(string $alias)
    {
        $configs = $this->getSchemaForScenario($alias);
        $sort = $this->resolve($configs['sort']);
        return $sort instanceof CompositeInterface
            ? $this->fillComposite($sort, $configs['items'] ?? [])
            : $sort;

    }

    /**
     * @param CompositeInterface $composite
     * @param array $items
     * @return SortInterface
     */
    private function fillComposite(CompositeInterface $composite, array $items): SortInterface
    {
        foreach ($items as $item) {
            $composite->addSort($this->resolve($item));
        }

        /**
         * @var SortInterface $composite
         */
        return $composite;
    }

    /**
     * @param string $alias
     * @return mixed
     */
    private function getSchemaForScenario(string $alias)
    {
        return $this->schema[$alias] ?? $this->schema[self::DEFAULT_SCENARIO];
    }

    /**
     * @param string $className
     * @return SortInterface
     */
    private function resolve(string $className): SortInterface
    {
        return $this->container->build($className);
    }
}