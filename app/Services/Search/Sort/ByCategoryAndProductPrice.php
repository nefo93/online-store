<?php

namespace App\Services\Search\Sort;

use App\Services\Search\Composite\Contracts\SortInterface;
use Illuminate\Support\Collection;

class ByCategoryAndProductPrice implements SortInterface
{

    public function sort(Collection $collection)
    {
         return $collection->sortBy(function ($collection){
             return [$collection->category_id, $collection->price];
         });
    }


}