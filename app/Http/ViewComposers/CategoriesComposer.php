<?php
namespace App\Http\ViewComposers;

use App\Model\Category;
use Illuminate\View\View;

class CategoriesComposer
{
    private $categoryModel;

    public function __construct(Category $category)
    {
        $this->categoryModel = $category;
    }

    public function compose(View $view)
    {
        $categories = $this->categoryModel->with('subCategory')->where('parent_id', null)->get();
        $view->with('categories', $categories);
    }
}