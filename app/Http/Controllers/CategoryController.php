<?php

namespace App\Http\Controllers;

use App\Model\Category;

class CategoryController extends Controller
{
    /**
     * @var Category
     */
    private $categoryModel;

    /**
     * CategoryController constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->categoryModel = $category;
    }


    public function show($slug)
    {
        $category = $this->categoryModel->getFirstSlug($slug);

        return view('Front.Categories.category', compact('category'));
    }

}
