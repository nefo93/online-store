<?php

namespace App\Http\Controllers;

use App\Model\Cart;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CartController extends Controller
{
    /**
     * get Cart Model
     *
     * @var Cart
     */
    protected $cart;

    /**
     * get Product Model
     *
     * @var Product
     */
    protected $product;

    /**
     * CartController constructor.
     * @param Cart $cart
     * @param Product $product
     */
    public function __construct(Cart $cart, Product $product)
    {
        $this->cart = $cart;
        $this->product = $product;
    }

    /**
     * add product in cart session
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function AddToCart(Request $request, $slug)
    {
        $product = $this->product->GetFirstSlug($slug);//get product on slug
        $oldCart = Session::has('cart') ? Session::get('cart') : null; // get session 'Cart'
        $this->cart->setSetting($oldCart);// set config session in class App\Model\Cart
        $this->cart->add($product, $product->id); // we add a new product to the session cart
        $request->session()->put('cart', $this->cart);

        return redirect()->back();
    }

    /**
     * show all product in cart session
     *
     * @return view
     */
    public function ShowCart()
    {
        if(!Session::has('cart')) {
            return view('Front.Cart.index');
        }
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart->setSetting($oldCart);

        return view('Front.Cart.index', [
            'products' => $this->cart->items,
            'totalPrice' => $this->cart->totalPrice,
            ]);
    }

    /**
     * delete one product in cart session
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function DeleteOneItemCart(Request $request, $slug)
    {
        $product = $this->product->GetFirstSlug($slug); //get product on slug
        $oldCart = Session::has('cart') ? Session::get('cart') : null;// get session 'Cart'
        $this->cart->setSetting($oldCart);// set config session in class App\Model\Cart

        /**
         * We delete the product from the session.
         * If it returns true, we completely delete the session.
         * If false, then we delete only 1 product.
         */
        if($this->cart->deleteOne($product->id)) {
            $request->session()->forget('cart');
        } else {
            $request->session()->put('cart', $this->cart);
        }

        return redirect()->back();
    }

    /**
     * delete the session with the name 'cart'
     *
     * @param Request $request
     * @return Redirector
     */
    public function DeleteAllItemCart(Request $request)
    {
        $request->session()->forget('cart');// delete all session cart

        return redirect('/');
    }
}
