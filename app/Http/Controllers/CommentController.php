<?php

namespace App\Http\Controllers;

use App\Model\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * @var Comment
     */
    private $commentModel;

    public function __construct(Comment $comment)
    {
        $this->commentModel = $comment;
    }

    public function store(Request $request)
    {
        Validator::make($request->all(),[
            'name' => 'alpha',
            'email' => 'email',
            'comment' => 'required'
        ])->validate();


        if(Auth::user()) {
            $this->commentModel->user_id = Auth::user()->id;
        }

        $this->commentModel->product_id = $request['product_id'];
        $this->commentModel->comment = $request['comment'];
        $this->commentModel->name = $request['name'];
        $this->commentModel->email = $request['email'];
        $this->commentModel->parent_id = $request['parent_id'];
        $this->commentModel->save();

        return redirect()->back();
    }
}
