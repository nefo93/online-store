<?php

namespace App\Http\Controllers;

use App\Services\Search\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $service;

    public function __construct(SearchService $service)
    {
        $this->service = $service;
    }

    public function store(Request $request)
    {
        $search = $request->search ? $request->search : '';
        $products = $this->service->collect($search);

        return view('Front.Search.index', compact('products'));
    }
}
