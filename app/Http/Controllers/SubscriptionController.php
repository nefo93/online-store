<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Stripe;

class SubscriptionController extends Controller
{

    public function store(Request $request)
    {
        Stripe::setApiKey(getenv('STRIPE_SECRET'));

        $token = $request['stripeToken'];

        $customer = \Stripe\Customer::create([
            "email" => $request['stripeEmail'],
            "source" => $token,
        ]);

        $charge = Charge::create([
            'amount' => 100,
            'currency' => 'usd',
            'customer' => $customer->id,
        ]);

//        dd($charge);
        return redirect()->back();
    }
}
