<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * @var Product
     */
    private $productModel;

    /**
     * HomeController constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->productModel = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productModel->StatusTrues;
        return view('Front.index', compact('products'));
    }

}
