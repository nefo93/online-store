<?php

namespace App\Http\Controllers;

use App\Model\Product;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $productModel;

    /**
     * ProductController constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->productModel = $product;
    }

    public function show($slug)
    {
        $product = $this->productModel->GetFirstSlug($slug);

        return view('Front.Products.product', compact('product'));
    }
}
