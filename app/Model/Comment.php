<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'user_id',
        'products_id',
        'comment',
        'name',
        'email'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function parentsComment()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function subComment()
    {
        return $this->belongsTo(Comment::class, 'parent_id');
    }
}
