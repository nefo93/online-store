<?php

namespace App\Model;


use Illuminate\Support\Facades\Request;

class Cart
{
    /**
     * @var null
     */
    public $items = null;

    /**
     * @var int
     */
    public $totalQty = 0;

    /**
     * @var int
     */
    public $totalPrice = 0;

    /**
     * @param $oldCart
     */
    public function setSetting($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    /**
     * add new product
     *
     * @param $item
     * @param $id
     */
    public function add($item, $id)
    {
        $storedItem = ['qty' => 0, 'price' => $item->price, 'item' => $item];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $item->price;
    }

    /**
     * delete 1 product
     *
     * @param $id
     * @return bool
     */
    public function deleteOne($id)
    {
        if($this->totalQty == 1) {
            return true;
        }
        $storedItem = $this->items[$id];
        if($storedItem['qty'] <= 1) {
            unset($this->items[$id]);
        } else {
            $storedItem['qty']--;
            $storedItem['price'] -= $storedItem['item']->price;
            $this->items[$id] = $storedItem;
        }

        $this->totalQty--;
        $this->totalPrice -= $storedItem['item']->price;
        return false;
    }

}