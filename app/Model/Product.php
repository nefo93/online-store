<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Product extends Model
{
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'price',
        'status',
        'quantity'
    ];

    /**
     * Receiving product where status is true or false
     *
     * @return mixed
     */
    public function getStatusTruesAttribute()
    {
        return $this->where('status', true)->get();
    }

    /**
     * @param Builder $query
     * @param $slug
     * @return mixed
     */
    public function scopeGetFirstSlug(Builder $query, $slug)
    {
        return $this->where('slug', $slug)->first();
    }

    /**
     * Price change while saving to base
     *
     * @param $price
     */
    public function setPriceAttribute($price)
    {
        $this->attributes['price'] = $price*100;
    }

    /**
     * Changing the description while saving to the database. We get html tags
     *
     * @param string $description
     */
    public function setDescriptionAttribute(string $description)
    {
        $this->attributes['description'] = strip_tags($description);
    }

    /**
     *Changes a string to lower case
     *
     * @param string $slug
     */
    public function setSlugAttribute(string $slug)
    {
        $this->attributes['slug'] = mb_strtolower($slug);
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageble');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}
