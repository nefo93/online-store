<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

    /**
     *Changes a string to lower case
     *
     * @param string $slug
     */
    public function setSlugAttribute(string $slug)
    {
        $this->attributes['slug'] = mb_strtolower($slug);
    }

    /**
     * @param Builder $query
     * @param $slug
     * @return mixed
     */
    public function scopeGetFirstSlug(Builder $query, $slug)
    {
        return $this->where('slug', $slug)->first();
    }

    /**
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeParent(Builder $query)
    {
        return $qoery->whereNull('parent_id');
    }

    /**
     * @return BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return HasMany
     */
    public function subCategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * @return MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageble');
    }
}
