<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'src',
        'imageble_id',
        'imageble_type'
    ];

    public function imageble()
    {
        return $this->morphTo();
    }
}
