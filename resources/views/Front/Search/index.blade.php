@extends('layouts.app')

@section('content')
    <h1 class="text-center">Search</h1>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th>Name</th>
            <th>Category</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->category->name }}</td>
            <td>{{ $product->price }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection