@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1 class="align-content-center">Популярные продукты</h1>
            <ul class="products">
                @foreach($products as $product)
                <li class="li">
                    <a href="{{ url('product/'.$product->slug) }}">
                    <div class="img" style="background-image: url({{ asset( $product->images->first()->src ) }});">
                        <p class="text">
                            {{ $product->name }}
                        </p>
                    </div>
                    </a>
                    <a href="add-to-cart/{{ $product->slug }}" class="btn btn-primary cart">
                        <i class="fa fa-shopping-cart"></i>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection