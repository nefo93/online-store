@extends('layouts.app')

@section('content')
    <h1 class="text-center">{{ trans('file.CartName') }}</h1>
    @if(Session::has('cart'))
        <?php $count = 1; ?>
        <table class="table text-center">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
                <th scope="col">Delete 1</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            <tr>
                <th scope="row">{{ $count++ }}</th>
                <td>{{ $product['item']['name'] }}</td>
                <td>{{ $product['qty'] }}</td>
                <td><span class="badge badge-success">{{ $product['price']/100 }} $</span></td>
                <td>
                    <a href="{{ url('delete-to-cart/'.$product['item']['slug']) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
            <tr>
                <td>Total: <span class="font-weight-bold"><span class="badge badge-success">{{ $totalPrice/100 }} $</span></td>
            </tr>
            </tbody>
        </table>
        <div class="text-right">
            <a href="{{ url('delete-all-to-cart') }}" class="btn btn-danger">Delete all</a>
            <a href="#" class="btn btn-success">Checkout</a>
        </div>
    @else
        <div>
            <h1>{{ trans('file.CartText') }}</h1>
        </div>
    @endif
@endsection