@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="product">
            <div class="row mb-2">
                <div class="col-md-7 img" style="background-image: url('{{ asset($product->images->first()->src ) }}')"></div>
                <div class="col-md-5">
                    <h1>{{ $product->name  }}</h1>
                    <p><span class="text-primary">Category:</span> {{ $product->category()->first()->name }}</p>
                    <p><span class="text-primary">Цена:</span> {{ $product->price/100 }} $</p>
                    <p><span class="text-primary">Статус:</span>
                        @if($product->status)
                            <i class="fas fa-check"></i>
                        @else
                            <i class="fas fa-times"></i>
                        @endif
                    </p>
                    <p><span class="text-primary">Quantity:</span> {{ $product->quantity }}</p>
                </div>
            </div>
            <div>
                <p>
                    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                </p>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ url('/comment') }}" method="POST" class="mb-3">
                {{ csrf_field() }}
                @if(!Auth::user())
                    <div class="form-inline">
                        <label for="name" class="mr-2">Name:</label>
                        <input type="text" class="form-control mr-3" id="name" name="name">
                        <label for="email" class="mr-2">Email:</label>
                        <input type="email" class="form-control" name="email" id="email">
                    </div>
                @endif
                <input type="number" value="{{ $product->id }}" class="d-none" name="product_id">
                <input type="number" name="parent_id" class="d-none" id="parent_id" value="">
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
                </div>
                <input type="submit" class="btn btn-success">
            </form>
            <ul class="list-group">

            @foreach($product->comment->reverse() as $comment)
                <span class="idComment mb-3" data-index-number="{{ $comment->id }}">
                    <li class="list-group-item active">
                        <span>
                            <span>Name: {{ $comment->user? $comment->user->name: $comment->name }}</span>
                            <span class="float-right">Data: {{ $comment->updated_at }}</span>
                        </span>
                    </li>
                    <li class="list-group-item">{{ $comment->comment }}</li>
                </span>
                @foreach($comment->parentsComment as  $parent)
                <span class="idComment ml-5 mb-3" data-index-number="{{ $parent->id }}">
                    <li class="list-group-item active">
                        <span>
                            <span>Name: {{ $parent->user? $parent->user->name: $parent->name }}</span>
                            <span class="float-right">Data: {{ $parent->updated_at }}</span>
                        </span>
                    </li>
                    <li class="list-group-item">{{ $parent->comment }}</li>
                </span>
                @endforeach
            @endforeach
            </ul>
        </div>
    </div>
@endsection