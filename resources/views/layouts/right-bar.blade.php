<div id="accordion">
    <ul class="list-group">
        @foreach($categories as $category)
            <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info" data-toggle="collapse" data-target="#{{ $category->slug }}">
                {{ $category->name }}
                <span class="badge badge-primary badge-pill">{{ $category->subCategory()->count() }}</span>
            </li>
            @if($category->subCategory->count())
                <div class="collapse list-group

                                    @foreach($category->subCategory as $parent)
                {{ Request::is('category/'.$parent->slug)? 'show': '' }}
                @endforeach

                        " id="{{ $category->slug }}" data-parent="#accordion">

                    @foreach($category->subCategory as $parent)
                        <a href="/category/{{ $parent->slug }}" class="list-group-item list-group-item-action {{ Request::is('category/'.$parent->slug)? 'active': '' }}">
                            {{ $parent->name }}
                            <span class="badge badge-danger badge-pill float-right mt-1">{{ $parent->products->count() }}</span>
                        </a>
                    @endforeach

                </div>
            @endif
        @endforeach
    </ul>
</div>