<?php

return [
    'Translations' => 'Translations',
    'CartName' => 'Cart',
    'CartText' => 'No item in cart',
    'Login' => 'Login',
    'Register' => 'Register',
    'Logout' => 'Logout',
    'Subscription' => 'Subscription'
];