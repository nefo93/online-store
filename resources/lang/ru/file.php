<?php

return [
    'Translations' => 'Перевод',
    'CartName' => 'Корзина',
    'CartText' => 'Корзина пустая',
    'Login' => 'Авторизация',
    'Register' => 'Ругистрация',
    'Logout' => 'Выход',
    'Subscription' => 'Подписка'
];